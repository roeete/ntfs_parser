from volume.volume import Volume
from volume.vbr import VBR
from mft.mft import MFT
from attributes.attribute import Attribute
# CR: [finish] Import only what you need
import logging
from time import time
# CR: [performance] Your code seems to be highly inefficient
# CR: [general] Missing a .gitignore file

# CR: [conventions] Keep a line boundary of 80 characters
# CR: [conventions] Class members should begin with '_' to mark them as
# private (The same goes for private methods). Classes that represent data
# structures are an exception since their members should be public.
# CR: [conventions] Functions should appear in the order of usage
# CR: [finish] Remove comments

# CR: [design] LINE_SEPERATOR and the print functions don't belong in the entry
# point of your program.

LINE_SEPERATOR = "------------------------"


def print_match(match):
    # CR: [finish] Recommending f-strings
    print("MFT Entry {}, FileName: {}".format(match.address, match.FileName))
    print("Data:\n" + LINE_SEPERATOR)
    print(match.Data)
    print(LINE_SEPERATOR)


def print_matches(matches):
    if len(matches) == 0:
        print("No Match!")
        return

    if len(matches) == 1:
        print("Match\n" + LINE_SEPERATOR)
        print_match(matches[0])
        return

    print("Multiple Matches")
    for ind, match in enumerate(matches):
        print("Match {}\n".format(ind) + LINE_SEPERATOR)
        print_match(match)


# CR: [design] This function is way too long and seem to do things in many
# abstraction levels.
def main(args=None):
    ''''# Attribute 16 Creation took: 0.0009975433349609375
    attribute_16_that_took_time_creating = b'\x10\x00\x00\x00`\x00\x00\x00\x00\x00\x18\x00\x00\x00\x00\x00H\x00\x00\x00\x18\x00\x00\x00\xa9-a\xce*\xfc\xd1\x01\xa9-a\xce*\xfc\xd1\x01\xa9-a\xce*\xfc\xd1\x01\xa9-a\xce*\xfc\xd1\x01\x06\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x000\x00\x00\x00p\x00\x00\x00\x00\x00\x18\x00\x00\x00\x02\x00R\x00\x00\x00\x18\x00\x01\x00\x05\x00\x00\x00\x00\x00\x05\x00\xa9-a\xce*\xfc\xd1\x01\xa9-a\xce*\xfc\xd1\x01\xa9-a\xce*\xfc\xd1\x01\xa9-a\xce*\xfc\xd1\x01\x00\x10\x00\x00\x00\x00\x00\x00\x00\x10\x00\x00\x00\x00\x00\x00\x06\x00\x00\x00\x00\x00\x00\x00\x08\x03$\x00M\x00F\x00T\x00M\x00i\x00r\x00r\x00\x00\x00\x00\x00\x00\x00\x80\x00\x00\x00H\x00\x00\x00\x01\x00@\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00@\x00\x00\x00\x00\x00\x00\x00\x00\x10\x00\x00\x00\x00\x00\x00\x00\x10\x00\x00\x00\x00\x00\x00\x00\x10\x00\x00\x00\x00\x00\x00\x11\x01\x02\x00\x00\x00\x00\x00\xff\xff\xff\xff\x00\x00\x00\x00\x12\x00\x00\x00\x01\x02\x00\x00\x00\x00\x00\x05 \x00\x00\x00 \x02\x00\x00\x00\x00\x00\x00\x80\x00\x00\x00H\x00\x00\x00\x01\x00@\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00@\x00\x00\x00\x00\x00\x00\x00\x00\x10\x00\x00\x00\x00\x00\x00\x00\x10\x00\x00\x00\x00\x00\x00\x00\x10\x00\x00\x00\x00\x00\x00\x11\x01\x02\x00\x00\x00\x00\x00\xff\xff\xff\xff\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x90\x05\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x90\x05'
    attribute_creation_start = time()
    attr = Attribute.create_attribute(attribute_16_that_took_time_creating)
    attribute_creation_end = time()
    print(f"Main: Attribute {attr.header.type_id} Creation took: {attribute_creation_end - attribute_creation_start}")
    exit(1)'''

    # CR: [general] +1 for using logging. You could also add a way to control
    # logging by the user.
    logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG)
    # CR: [requirements] This should be suppplied by the user
    volume_path = r"\\.\C:"
    # CR: [finish] Don't shorten words. Code should be easy to read aloud
    with open(volume_path, "rb") as vol_file:
        # CR: [implementation] This is redundant. Files are opened at offset 0
        vol_file.seek(0, 0)
        vbr_raw_data = vol_file.read(512)
        # CR: [design] Is the vbr not part of the volume? Shouldn't it be
        # instantiated inside?
        vbr = VBR(vbr_raw_data, vol_file)

        vol = Volume(vol_file, vbr)
        # CR: [finish] This is a great example for a line that's hard to
        # understand: What is clus? what is lcn?
        # CR: [design] Extract low level operations to a function
        vol_file.seek(vbr.clus_to_bytes(vbr.mft_lcn), 0)
        mft_entry_raw_data = vol_file.read(vbr.bytes_per_mft_entry)

        # CR: [design] Is the mft not part of the volume? Shouldn't it be
        # instantiated inside?
        print("Processing MFT")
        mft = MFT(mft_entry_raw_data, vol)
        print("\n------------------------\nSearch for File\n------------------------")
        # CR: [design] This loop is very unintuitive. If search can return
        # multiple matches, does it not return all of them? And if it doesn't,
        # why not return one match at a time? Make your interfaces easy to use
        # correctly and hard to use incorrectly.
        '''while True:
            # CR: [finish] Don't leave comments lying around
            # wanted_file = input("File Name: ")
            # CR: [requirements] This should be suppplied by the user
            wanted_file = "c.txt"
            matches = mft.search(wanted_file)
            print_matches(matches)
            if input("Continue? (y/n): ") == "n":
                break'''


if __name__ == '__main__':
    main()
