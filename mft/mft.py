from common.data_structures import calcSize, MFT_HEADER
from common.raw_data_parser import RawDataParser
# CR: [implementation] hex_dump and to_bytes are unused, remove
from common.utils import hex_dump, take_bytes, to_bytes
from common.converter import integer_converter

from attributes.attribute import Attribute

from collections import namedtuple
import logging
from time import time


# CR: [finish] This is unused. Remove
class Fixup:
    def __init__(self, raw_data):
        self.signature = raw_data[:2]
        self.fixup_arr = raw_data[2:]


class MFTHeader(RawDataParser):
    def __init__(self, raw_data):
        super(MFTHeader, self).__init__(raw_data)


def create_attr_timeit(raw):
    attribute_creation_start = time()
    attr = Attribute.create_attribute(raw)
    attribute_creation_end = time()
    print(f"  Time it Attribute {attr.header.type_id} Creation took: {attribute_creation_end - attribute_creation_start}")
    return attr


# TODO: split this to File class and MFTEntry class
class MFTEntry:
    END_MARKER = -1
    AttributeIdentifier = namedtuple("AttributeIdentifier", ["name", "id"])

    # CR: [design] Why does the entry have access to the mft?
    def __init__(self, mft, address, raw_data):
        self.mft = mft
        self.address = address
        self.attributes = {}

        # Process Header and Make sure it is not corrupted

        start = time()
        self.header = MFTHeader(raw_data[:calcSize(MFT_HEADER)])  # This takes no time
        end = time()
        # print(f" Overall MFT Entry Header Creation took: {end-start}")

        if not self.valid:
            # CR: [finish] Why is this commented out?
            # logging.warning("MFT Entry {} is corrupted (signature is not 'FILE')".format(address))
            return

        self.raw_data = raw_data  # Offset to first attribute is relative to the start of the entry: including MFTHeader

        start = time()
        # self.process_fixup()  # This takes no time
        end = time()
        # print(f" Overall MFT Entry fixup processing took: {end-start}")

        start = time()
        self.process_attributes()
        end = time()
        # print(f" Overall MFT Attributes Processing took: {end-start}")

    @property
    def valid(self):
        return self.header.signature == b'FILE'

    # CR: [finish] This function is way too complex
    def process_fixup(self):
        raw_data = bytearray(self.raw_data)
        offset = self.header.offset_to_fixup
        sig = self.raw_data[offset:offset + 2]
        length_of_fixup_in_bytes = (self.header.number_of_fixup_entries - 1) * len(sig) * 2  # Number of fixup entries includes signature
        fixups = self.raw_data[offset + len(sig):offset + length_of_fixup_in_bytes]
        for fixup_ind in range(self.header.number_of_fixup_entries - 1):
            curr_sec_offset = (fixup_ind + 1) * self.mft.vol.vbr.bytes_per_sector
            if self.raw_data[curr_sec_offset-len(sig):curr_sec_offset] != sig:
                raise Exception("Sector is damaged (Fixup check)!")
            raw_data[curr_sec_offset-len(sig):curr_sec_offset] = fixups[fixup_ind * len(sig):fixup_ind * len(sig) + len(sig)]
        self.raw_data = bytes(raw_data)

    # CR: [finish] This function doesn't process the entire entry, only its
    # attributes
    def process_attributes(self):
        attrs_raw = self.raw_data[self.header.first_attr_offset:]
        curr_off = 0
        # CR: [design] This is a great example for non-modularity: The MftEntry
        # has an MFT (weird), which has a vol (is it a volume? again weird)
        # which has a VBR (third time weird). Not only is the structure
        # confusing, it is also unencapsulated! The rule is that you are only
        # allowed to access public members or methods of your own members.

        times_sum_for_attributes = 0
        while curr_off < self.mft.vol.vbr.bytes_per_mft_entry:
            attribute_processing_start = time()
            # CR: [design] This check should be encapsulated in the attributes
            if integer_converter(take_bytes(attrs_raw, curr_off, 4), signedFlag=True) == MFTEntry.END_MARKER:
                break

            attribute_creation_start = time()
            attribute_length, attribute = Attribute.create_attribute(attrs_raw[curr_off:], self.mft.vol)
            attribute_creation_end = time()
            # print(f"   {attribute} Creation took: {attribute_creation_end - attribute_creation_start}")
            curr_off += attribute_length
            # CR: [implementation] This is hard to understand
            if attribute is not None:
                self.attributes[MFTEntry.AttributeIdentifier(attribute.__class__.__name__, attribute.header.id)] = attribute
            attribute_processing_end = time()
            # print(f"   Processing of attribute id {attribute} took: {attribute_processing_end-attribute_processing_start}")
            times_sum_for_attributes += attribute_processing_end-attribute_processing_start
        # print(f"  Sum of attributes processing is: {times_sum_for_attributes}")
    # CR: [finish] This is confusing with python's getattr.
    # get_attribute_by_info would be better. Also req could be renamed to
    # attribute_info.

    def get_attr(self, req):
        for attr_id, attr in self.attributes.items():
            if attr_id.name == req.name and (req.id is None or req.id == attr_id.id):
                return attr
        return None

    # TODO: Move this to File Class and implement it
    # CR: [finish] methods and members should be lower snake case
    @property
    def FileName(self):
        filename_attr = self.get_attr(MFTEntry.AttributeIdentifier("FileName", None))
        if filename_attr is None:
            return "$NO_NAME"
        return filename_attr.FileName

    @property
    def file_content(self):
        file_data_attr = self.get_attr(MFTEntry.AttributeIdentifier("Data", None))
        if file_data_attr is None:
            return b""
        return file_data_attr.data

    # TODO: 1. Move this to File Class. 2. Implement ADS 3. Implement Encoding by extension
    @property
    # CR: [finish] methods and members should be lower snake case
    # CR: [design] What is the return value? bytes? str?
    def Data(self):
        # All of this should happen *inside* attribute, once we grant it volume reading capabilities
        '''
        if not file_data_attr.header.non_resident_flag:
            return file_data_attr.data.raw_data

        # CR: [implementation] I don't understand if non-resident data is
        # supported or not. Why do you perform logic if it isn't?
        file_raw = bytes()
        for cluster in file_data_attr.data.clusters:
            file_raw += bytes(self.mft.vol.read_cluster(cluster))
        logging.error("Non-Resident Data not implemented")
        return file_raw.decode(encoding="UTF-8", errors="ignore")
        '''
        return self.file_content


class MFT:
    '''
    TODO:
        1. Implement Non-base record processing!
        2. Implement MFT's $BITMAP Attribute and such, this will greatly improve efficeny
    '''
    MFT_FILE_ADDRESS = 0

    def __init__(self, mft_entry_raw_data, vol):
        # CR: [finish] Remove call to super
        super(MFT, self).__init__()
        self.vol = vol

        # CR: [implementation] This line is confusing let's talk about it
        self._mft_entry = MFTEntry(self, MFT.MFT_FILE_ADDRESS, mft_entry_raw_data)
        self.process_mft()

    @property
    def mft_file(self):
        return self._mft_entry

    @property
    def file_entries(self):
        file_entry_size = self.vol.vbr.bytes_per_mft_entry  # replace self.vol.vbr.bytes_per_mft_entry with volume_info.bytes_per_mft_entry
        file_entries_raw = self.mft_file.file_content
        for file_entry_offset in range(0, len(file_entries_raw), file_entry_size):
            yield file_entries_raw[file_entry_offset:file_entry_offset+file_entry_size]

    def process_mft(self):
        # TODO:
        #  Don't process the entire mft, process until we reach the desired file!! and than stop. Change this

        # CR: [design] You seem to be repeating the processing of a data
        # attribute, which you should already have done somewhere else to get
        # a file's content

        files_counter = 0
        sum_of_file_times = 0
        mft_processing_start = time()
        for file_entry in self.file_entries:
            file_entry_start = time()
            curr_file = MFTEntry(self, files_counter, file_entry)  # TODO: do this better
            files_counter += 1
            file_entry_end = time()
            # print(f" Processing of File Entry {files_counter}, FileName: {curr_file.FileName}, took: {file_entry_end - file_entry_start}")
            sum_of_file_times += file_entry_end - file_entry_start

        mft_processing_end = time()

        mft_processing_time = mft_processing_end - mft_processing_start
        print(f"MFT Processing took: {mft_processing_time}")
        print(f"Number of file entries: {files_counter}")
        print(f"sum_of_file_times: {sum_of_file_times}")
        print(f"Average File Entry processing time: {mft_processing_time/files_counter}")
        print(f"Average by sum: {sum_of_file_times/files_counter}")
