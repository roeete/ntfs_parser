from enum import Enum
from math import pow
# CR: [conventions] Import order should be longest to shortest
from collections import namedtuple
from time import time
# CR: [requirements] Are you allowed to assume this?
ENDIANESS = "little"
# CR: [design] Why is this a thing? What is wrong with int and choosing one
# unit of size (bytes for example)? Keep it simple stupid.
RecordSize = namedtuple("RecordSize", ["type", "size"])


# TODO: Add flags category for different flags ($FILE_NAME flags, $MFT flags etc.)


class RecordSizeTypes(Enum):
    clusters = 1
    # CR: [implementation] Don't shadow type names
    bytes = 2
    unknown = 3


# CR: [finish] Rename to signed_flag
# CR: [design] Why have signedFlag as parameter if it can never be changed?
def integer_converter(raw_data, signedFlag=False):
    # integer_converter_start = time()
    converted = int.from_bytes(raw_data, ENDIANESS, signed=signedFlag)
    # integer_converter_end = time()
    # print(f"  Integer convertion took: {integer_converter_end-integer_converter_start}")
    return converted


# CR: [finish] Be consistent with naming: record_size_converter
def record_size(raw_data):
    data = int.from_bytes(raw_data, ENDIANESS, signed=True)
    if data < 0:
        return RecordSize(RecordSizeTypes.bytes, pow(2, abs(data)))
    return RecordSize(RecordSizeTypes.clusters, data)


# CR: [finish] Being the default doesn't imply it does nothing. Rename to
# bytes_converter.
def default_converter(raw_data):
    return raw_data


# CR: [implementation] Use lambdas for simplicity
converters = {
    "int": integer_converter,
    "record_size": record_size,
    "timestamp": integer_converter,
    "ref": integer_converter,
    "string": default_converter,
    "default": default_converter
}
