from collections import namedtuple
from common.converter import converters

StructreEntry = namedtuple("StructreEntry", ["offsets", "converter"])

# CR: [design] You could provide only the size instead of offsets and have the
# RawDataParser keep track of the offset. You could provide only a python
# built-in type and have RawDataParser find the conversion function based on
# it. Or you could have used struct unpack and give up parsing by type if it's
# not cost effective.

# TODO: Do this better, by specifing offset in hex and only offset!
# TODO: Timestamps converters

# ------------------------------------------------------------------
# MFT Logic
# ------------------------------------------------------------------

MFT_HEADER = {
    "SIGNATURE":                            StructreEntry((0, 3), converters["string"]),
    "OFFSET_TO_FIXUP":                      StructreEntry((4, 5), converters["int"]),
    "NUMBER_OF_FIXUP_ENTRIES":              StructreEntry((6, 7), converters["int"]),
    "LSN":                                  StructreEntry((8, 15), converters["int"]),
    "SEQ_VAL":                              StructreEntry((16, 17), converters["int"]),
    "LINK_COUNT":                           StructreEntry((18, 19), converters["int"]),
    "FIRST_ATTR_OFFSET":                    StructreEntry((20, 21), converters["int"]),
    "FLAGS":                                StructreEntry((22, 23), converters["default"]),  # TODO: Add Flags Converter
    "USED_SIZE":                            StructreEntry((24, 27), converters["int"]),
    "ALLOCATED_SIZE":                       StructreEntry((28, 31), converters["int"]),
    "REF_TO_BASE_ENTRY":                    StructreEntry((32, 39), converters["int"]),
    "NEXT_ATTR_ID":                         StructreEntry((40, 41), converters["int"])
}

# ------------------------------------------------------------------
# Attribute
# ------------------------------------------------------------------

ATTRIBUTE_HEADER = {
    "TYPE_ID":                              StructreEntry((0, 3), converters["int"]),
    "ATTR_LEN":                             StructreEntry((4, 7), converters["int"]),
    "NON_RESIDENT_FLAG":                    StructreEntry((8, 8), converters["int"]),
    "NAME_LEN":                             StructreEntry((9, 9), converters["int"]),
    "NAME_OFFSET":                          StructreEntry((10, 11), converters["int"]),
    "FLAGS":                                StructreEntry((12, 13), converters["default"]),  # TODO: Add Flags Converter
    "ID":                                   StructreEntry((14, 15), converters["int"])
}

RESDIENT_ATTRRIBUTE_HEADER = {
    "SIZE_OF_CONTENT":                      StructreEntry((16, 19), converters["int"]),
    "OFFSET_TO_CONTENT":                    StructreEntry((20, 21), converters["int"])
}

NON_RESIDENT_ATTRIBUTE_HEADER = {
    "RUNLIST_STARTING_VCN":                 StructreEntry((16, 23), converters["int"]),
    "RUNLIST_ENDING_VCN":                   StructreEntry((24, 31), converters["int"]),
    "RUNLIST_OFFSET":                       StructreEntry((32, 33), converters["int"]),
    "COMPRESSION_UNIT_SIZE":                StructreEntry((34, 35), converters["int"]),
    "UNSED":                                StructreEntry((36, 39), converters["default"]),
    "ALLOCATED_SIZE_OF_CONTENT":            StructreEntry((40, 47), converters["int"]),
    "ACTUAL_SIZE_OF_CONTENT":               StructreEntry((48, 55), converters["int"]),
    "INITIALIZED_SIZE_OF_CONTENT":          StructreEntry((56, 63), converters["int"]),
}

# ------------------------------------------------------------------
# Attributes
# ------------------------------------------------------------------

FILE_NAME = {
    "FILE_REF_PARENT_DIR":                  StructreEntry((0, 7), converters["ref"]),
    "CREATION_TIME":                        StructreEntry((8, 15), converters["timestamp"]),  # TODO: Timestamp converter
    "MOD_TIME":                             StructreEntry((16, 23), converters["timestamp"]),
    "MFT_MOD_TIME":                         StructreEntry((24, 31), converters["timestamp"]),
    "FILE_ACCESS_TIME":                     StructreEntry((32, 39), converters["timestamp"]),
    "ALLOC_SZ_OF_FILE":                     StructreEntry((40, 47), converters["int"]),
    "REAL_SZ_OF_FILE":                      StructreEntry((48, 55), converters["int"]),
    "FLAGS":                                StructreEntry((56, 59), converters["int"]), # TODO: Flags Converter
    "REPARSE_VAL":                          StructreEntry((60, 63), converters["int"]),
    "NAME_LEN":                             StructreEntry((64, 64), converters["int"]),
    "NAMESPACE":                            StructreEntry((65, 65), converters["string"]),  # TODO: Namespace converter
}

STANDART_INFORMATION = {
    "CREATION_TIME":                        StructreEntry((0, 7), converters["timestamp"]),
    "MOD_TIME":                             StructreEntry((8, 15), converters["timestamp"]),
    "MFT_MOD_TIME":                         StructreEntry((16, 23), converters["timestamp"]),
    "FILE_ACCESS_TIME":                     StructreEntry((24, 31), converters["timestamp"]),
    "FLAGS":                                StructreEntry((32, 35), converters["default"]),
    "MAX_NUM_OF_VERS":                      StructreEntry((36, 39), converters["int"]),
    "VER_NUM":                              StructreEntry((40, 43), converters["int"]),
    "CLASS_ID":                             StructreEntry((44, 47), converters["int"]),
    "OWNER_ID":                             StructreEntry((48, 51), converters["int"]),
    "SID":                                  StructreEntry((52, 55), converters["int"]),
    "QUOTA_CHARGED":                        StructreEntry((56, 63), converters["int"]),
    "USN":                                  StructreEntry((64, 71), converters["int"]),

}

# TODO: Create Strucute class!!!!
BITMAP = False
DATA = False

# ------------------------------------------------------------------
# VBR Logic
# ------------------------------------------------------------------

# TODO: Parse zeros as sanity check only and don't parse not used
VBR = {
    "JMP":                                  StructreEntry((0, 2), converters["int"]),
    "OEM_ID":                               StructreEntry((3, 10), converters["default"]),
    "BYTES_PER_SECTOR":                     StructreEntry((11, 12), converters["int"]),
    "SECTORS_PER_CLUSTER":                  StructreEntry((13, 13), converters["int"]),
    "RESERVED_SECTORS":                     StructreEntry((14, 15), converters["int"]),
    "ZEROS_1":                              StructreEntry((16, 18), converters["default"]),
    "NOT_USED_1":                           StructreEntry((19, 20), converters["default"]),
    "MEDIA_DESCRIPTOR":                     StructreEntry((21, 21), converters["default"]),
    "ZEROS_2":                              StructreEntry((22, 23), converters["default"]),
    "SECTORS_PER_TRACK":                    StructreEntry((24, 25), converters["int"]),
    "NUMBER_OF_HEADS":                      StructreEntry((26, 27), converters["int"]),
    "HIDDEN_SECTORS":                       StructreEntry((28, 31), converters["int"]),
    "NOT_USED_2":                           StructreEntry((32, 39), converters["default"]),
    "TOTAL_SECTORS":                        StructreEntry((40, 47), converters["int"]),
    "MFT_LCN":                              StructreEntry((48, 55), converters["int"]),
    "MFT_MIRR_LCN":                         StructreEntry((56, 63), converters["int"]),
    "BYTES_PER_MFT_ENTRY":                  StructreEntry((64, 64), converters["record_size"]),
    "NOT_USED_3":                           StructreEntry((65, 67), converters["default"]),
    "BYTES_PER_INDEX_RECORD":               StructreEntry((68, 68), converters["record_size"]),
    "NOT_USED_4":                           StructreEntry((69, 71), converters["default"]),
    "VOLUME_SERIAL_NUMBER":                 StructreEntry((72, 79), converters["int"]),
    "CHECKSUM":                             StructreEntry((80, 83), converters["int"]),
    "BOOT_CODE":                            StructreEntry((84, 509), converters["default"]),
    "VBR_END_MARKER":                       StructreEntry((510, 511), converters["default"]),
}

# CR: [finish] Find a better name and don't shorten words!
DEF_ATTRIBUTE = {
    "NONE":                                 StructreEntry((0, 0), converters["default"])
}


# CR: [design] This should be a method of raw_data_parser (__len__)
def calcSize(data_structure):
    return list(data_structure.values())[len(data_structure.values()) - 1].offsets[1] + 1
