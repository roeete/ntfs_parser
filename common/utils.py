# CR: [general] The only function used from this file is take_bytes. Remove
# the rest.

def hex_dump(raw_data):
    hex_dump_output = "{}: ".format("0".rjust(8, '0'))
    bytes = list(map(lambda b: hex(b), raw_data))
    for byte_ind in range(0, len(bytes), 2):
        if byte_ind % 16 == 0 and byte_ind != 0:
            # hex_dump_output += " {}".format(0)
            print(hex_dump_output)
            hex_dump_output = "{}: ".format(str(byte_ind).rjust(8, '0'))
        hex_dump_output += "{}{} ".format(bytes[byte_ind][2:].ljust(2, '0'), bytes[byte_ind + 1][2:].ljust(2, '0'))
    print(hex_dump_output)


def to_bytes(raw):
    return list(map(lambda b: hex(b), raw))


def take_bytes(raw, offset, length):
    return raw[offset:offset+length]