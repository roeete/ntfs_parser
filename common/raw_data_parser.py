# CR: [conventions] Don't use star imports
from common.data_structures import *

# CR: [implementation] You could have each RawDataParser subclass define its
# own header as a class variable, and then access it through self. This will
# also mean that distributing the different structures into their relevant
# files, making it easier to reason about each class.
OBJ_TO_STRUCTURE = {
    "MFTHeader":            MFT_HEADER,
    "AttributeHeader":      ATTRIBUTE_HEADER,
    "ResidentHeader":       RESDIENT_ATTRRIBUTE_HEADER,
    "NonResidentHeader":    NON_RESIDENT_ATTRIBUTE_HEADER,
    "Attribute":            DEF_ATTRIBUTE,
    "VBR":                  VBR
}


class RawDataParser:
    def __init__(self, raw_data, struct=None):
        if raw_data is None:
            raise ValueError("Raw data cannot be None")
        # CR: [finish] This is unused. Remove
        self._initialized = False
        self.raw_data = raw_data
        if struct is None:
            self.structure = OBJ_TO_STRUCTURE[self.__class__.__name__]
        else:
            self.structure = struct

        # CR: [design] Can this condition be False at this point?
        if self.structure:  # TODO: Do this better with a DataStructure class
            self.parse_attrs()
        self._initialized = True

    @property
    def initialized(self):
        return self._initialized

    @staticmethod
    def prop_name(property_name):
        return property_name.lower()

    def parse_attrs(self):
        for attr_name, struct_entry in self.structure.items():
            setattr(self, RawDataParser.prop_name(attr_name),
                    struct_entry.converter(self.raw_data[struct_entry.offsets[0]:struct_entry.offsets[1] + 1]))

    def struct_entry(self, pos):
        return list(self.structure.values())[pos]
