# CR: [finish] Remove unused imports and don't use star imports
from common.raw_data_parser import RawDataParser
from common.data_structures import *
# CR: [implementation] Just use int.from_bytes directly
from common.converter import integer_converter
from common.utils import hex_dump, to_bytes, take_bytes
from collections import namedtuple
from time import time

from enum import Enum
import logging

# CR: [finish] Put inside FileName attribute class
WORD_SIZE = 2


class ResidentHeader(RawDataParser):
    def __init__(self, raw_data):
        super(ResidentHeader, self).__init__(raw_data)
        self.raw_data = raw_data


class NonResidentHeader(RawDataParser):
    def __init__(self, raw_data):
        super(NonResidentHeader, self).__init__(raw_data)
        self.raw_data = raw_data


class AttributeHeader(RawDataParser):
    NON_RESIDENT_LEN = calcSize(NON_RESIDENT_ATTRIBUTE_HEADER)
    RESDIENT_LEN = calcSize(RESDIENT_ATTRRIBUTE_HEADER)

    def __init__(self, raw_data):
        super(AttributeHeader, self).__init__(raw_data)
        self.raw_data = raw_data
        if self.non_resident_flag:
            self.extended = NonResidentHeader(self.raw_data)
        else:
            self.extended = ResidentHeader(self.raw_data)

    # CR: [design] This method seems like it is supposed to retrieve the size
    # of the AttributeHeader, but instead retrieves the size of other headers!
    @property
    def size(self):
        if self.non_resident_flag:
            return AttributeHeader.NON_RESIDENT_LEN
        return AttributeHeader.RESDIENT_LEN

    @property
    def known(self):
        return self.type_id in Attributes.keys()

    @property
    def is_resident(self):
        return not self.non_resident_flag


class Run:
    RunHead = namedtuple("RunHead", ["len_bytes", "off_bytes"])

    def __init__(self, header, length_in_clusters, offset):
        self.header = header
        # CR: [finish] Don't shadow built in names
        # CR: [finish] Instead of a comment call it length_in_clusters
        self.length_in_clusters = length_in_clusters
        self.vcn_offset = offset

    def __len__(self):
        return self.header.len_bytes + self.header.off_bytes + RunList.RUN_HEAD_SZ

    @property
    def valid(self):
        if self.vcn_offset != 0:
            return True
        return False

    @property
    def sparse(self):
        if self.length_in_clusters == 0:
            return True
        return False

    # CR: [finish] This name is not clear.
    # CR: [implementation] Why directly convert to a list?
    @property
    def clusters(self):
        return list(range(self.vcn_offset, self.vcn_offset + self.length_in_clusters))


class RunList:
    # CR: [finish] What is sz? What does this mean?
    RUN_HEAD_SZ = 1

    def __init__(self, raw):
        self._runs = []
        self.raw = raw

    @property
    def runs(self):
        curr_off = 0
        while True:
            curr_run = self.parse_run(curr_off)
            if curr_run is None:
                break
            self._runs.append(curr_run)
            yield curr_run
            curr_off += len(curr_run)

    def process_run(self):
        '''start = time()
        curr_off = 0
        while True:
            curr_run = self.parse_run(curr_off)
            if curr_run is None:
                break
            self.runs.append(curr_run)
            # CR: [design] Length of Run should be calculated in Run (__len__)
            curr_off += self.runs[-1].head.len_bytes + self.runs[-1].head.off_bytes + RunList.RUN_HEAD_SZ
        end = time()
        # print(f"    Processing of run took: {end - start}")'''
        return None

    # CR: [design] This should be part of Run initializer
    def parse_run(self, off):
        # CR: [implementation] Get nibbles using shifting and and
        run_head = hex(self.raw[off])[2:]
        if run_head == 'ff' or int(run_head, 16) == 0:
            return None
        # CR: [implementation] Just return None
        if len(run_head) < 2:
            # CR: [finish] Uncomment or remove
            # logging.warning("len(run_head) < 2, run_length is 0! This really shouldn't happen. Check this later")
            off_bytes = int(run_head, 16)
            len_bytes = 0
        else:
            off_bytes, len_bytes = int(run_head[:1], 16), int(run_head[1:2], 16)

        run_len = integer_converter(take_bytes(self.raw, off + RunList.RUN_HEAD_SZ, len_bytes))
        run_off = integer_converter(take_bytes(self.raw, off + len_bytes + RunList.RUN_HEAD_SZ, off_bytes), True)
        run_off = self.get_last_run_off() + run_off
        return Run(Run.RunHead(len_bytes, off_bytes), run_len, run_off)

    def get_last_run_off(self):
        if len(self._runs) == 0:
            return 0
        return self._runs[-1].vcn_offset

    @property
    def clusters(self):
        start = time()
        clusters = [cluster for run in self.runs for cluster in run.clusters]
        end = time()
        # print(f"Fetching of clusters for file took: {end - start}")
        return clusters


class ResidentData(RawDataParser):
    def __init__(self, struct, raw_data):
        super(ResidentData, self).__init__(raw_data, struct)
        self.raw_data = raw_data


class Attribute:
    def __init__(self, header, raw_data, volume):
        self.volume = volume  # TODO: replace volume with volume info, when the time is right
        self.header = header
        self.raw_data = raw_data
        # CR: [design] Data should be uniform to maintain abstraction. This
        # means it should probably hold the binary data itself. This also means
        # it shouldn't be computed on __init__, but only when requested.
        self._data = None

    @staticmethod
    def create_attribute(raw, volume):
        header = AttributeHeader(raw)  # TODO: It would probably be faster not to process the whole header when not needed...
        if not header.known:
            return header.attr_len, None
        return header.attr_len, Attributes[header.type_id](header, raw, volume)

    @property
    def data(self):
        if self.header.non_resident_flag:
            start = time()
            if self._data is None:
                self._data = self.get_non_resident_data()
            end = time()
            # print(f"Non-resident parsing of {self.header.type_id} took: {end - start}")
        else:
            start = time()
            if self._data is None:
                self._data = self.get_resident_data()
            end = time()
            # print(f"Resident parsing of {self.header.type_id} took: {end - start}")
        return self._data

    @property
    def is_resident(self):
        return self.header.is_resident

    def get_non_resident_data(self):
        if not self.header.known:
            return None
        data_content = b''
        for run in RunList(self.raw_data[self.header.extended.runlist_offset:]).runs:
            if run.valid and not run.sparse:
                # print(f"VCN Offset: {run.vcn_offset}, Length: {run.length_in_clusters}")
                current_chunk = self.volume.read_clusters(run.vcn_offset, run.length_in_clusters)
                data_content += current_chunk  # TODO: do this better with volume info
        return data_content

    def get_resident_data(self):
        if not self.header.known:
            return None
        return take_bytes(self.raw_data, self.header.extended.offset_to_content, self.header.extended.size_of_content)


class Data(Attribute):
    STRUCT = DEF_ATTRIBUTE

    def __init__(self, header, raw_data, volume):
        super(Data, self).__init__(header, raw_data, volume)


class FileName(Attribute):
    STRUCT = FILE_NAME
    NAME_OFFSET = list(STRUCT.values())[-1].offsets[1] + 1

    def __init__(self, header, raw_data, volume):
        super(FileName, self).__init__(header, raw_data, volume)

        self.attr_data = ResidentData(FileName.STRUCT, self.data)  # TODO: Talk about this with dan
        self._raw_name = None
        self._decoded_file_name = None

    @property
    def namespace(self):
        return integer_converter(self.data.namespace)

    @property
    def raw_name(self):
        if self._raw_name is None:
            self._raw_name = take_bytes(self.data, FileName.NAME_OFFSET, self.attr_data.name_len * WORD_SIZE)
        return self._raw_name

    @property
    def FileName(self):
        # CR: [finish] Remove comment, it doesn't add information
        # CR: [implementation] If you tell decode to ignore errors, how do you
        # expect errors to be raised?
        # CR: [implementation] Use a for loop with decoding schemes as items
        # CR: [implementation] If you don't use the exception's value, don't
        # use 'as e'
        # CR: [implementation] Wouldn't it be better to return an empty file
        # name?
        # Trying encoding using EAFP (Easier to Ask for Forgiveness than Permission)
        if self._decoded_file_name is None:
            self._decoded_file_name = self.raw_name.decode('UTF-16', errors="ignore")
        return self._decoded_file_name


# CR: [finish] typo: Standart -> Standard
class StandartInformation(Attribute):
    STRUCT = STANDART_INFORMATION

    def __init__(self, header, raw_data, volume):
        super(StandartInformation, self).__init__(header, raw_data, volume)


class Bitmap(Attribute):
    STRUCT = BITMAP

    def __init__(self, header, raw_data, volume):
        super(Bitmap, self).__init__(header, raw_data, volume)


class UnknownAttribute(Attribute):
    STRUCT = DEF_ATTRIBUTE

    def __init__(self, header, raw_data, volume):
        super(UnknownAttribute, self).__init__(header, raw_data, volume)


# CR: [finish] Hex values will be easier to reason about here
# CR: [finish] Attributes is a confusing name among all the classes. How about
# AttributeTypes?
# CR: [design] Should this be under a certain class?
Attributes = {
    0x30: FileName,
    0x80: Data,
}

'''
Attributes = {
    0: UnknownAttribute,
    16: StandartInformation,
    48: FileName,
    128: Data,
    176: Bitmap
}
'''