from common.raw_data_parser import RawDataParser
from common.converter import RecordSizeTypes
# CR: [conventions] Don't use star imports
# CR: [design] This is odd... Why would the VBR be dependent on things from the
# MFT?
# CR: [finish] Unused. remove
from mft.mft import *


class VBR(RawDataParser):
    def __init__(self, raw_data, vol):
        super(VBR, self).__init__(raw_data)
        # CR: [implementation] This is unused. Remove
        self.volume = vol
        self.process_record_sizes()

    @property
    def bytes_per_cluster(self):
        return self.sectors_per_cluster * self.bytes_per_sector

    def process_record_sizes(self):
        # TODO: consider this... I think this is bullshit and can be done in a better way
        if self.bytes_per_mft_entry.type == RecordSizeTypes.clusters:
            self.bytes_per_mft_entry = int(self.clus_to_bytes(self.bytes_per_mft_entry.size))
        else:
            self.bytes_per_mft_entry = int(self.bytes_per_mft_entry.size)

        if self.bytes_per_index_record.type == RecordSizeTypes.bytes:
            self.bytes_per_index_record = int(self.clus_to_bytes(self.bytes_per_index_record.size))
        else:
            self.bytes_per_index_record = int(self.bytes_per_index_record.size)

    # CR: [finish] Rename to function to clusters_to_bytes and argument to
    # clusters
    def clus_to_bytes(self, clus):
        return clus * self.bytes_per_cluster

    # CR: [design] Why should the VBR be responsible for fetching clusters?
    # CR: [finish] Rename vcn to offset_in_bytes
    # CR: [bug] You seek by cluster instead of by byte
    def get_cluster(self, vcn):
        self.vol.seek(vcn)
        return self.vol.read(self.bytes_per_cluster)
