class Volume:
    def __init__(self, vol, vbr):
        self.vol = vol
        self.vbr = vbr

    # CR: [general] Don't shadow type names. size is a better name anyway
    def read_bytes(self, offset_to_seek, bytes_to_read):
        self.vol.seek(offset_to_seek)
        return self.vol.read(bytes_to_read)

    # CR: [finish] This is unused. Remove
    # CR: [design] Avoid code duplication!
    def read_cluster(self, vcn):
        self.vol.seek(vcn * self.vbr.bytes_per_cluster)
        return self.vol.read(self.vbr.bytes_per_cluster)

    def read_clusters(self, vcn, num):
        return self.read_bytes(vcn * self.vbr.bytes_per_cluster, self.vbr.bytes_per_cluster * num)
